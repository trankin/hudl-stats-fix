fs = require('fs');
Papa = require('papaparse');


// Expected command
// node index.js <numberofsets> <inputCSVfile>
// outputs <inputCSVfile>.txt in maxpreps format




var inFile = process.argv[3];
var setsPlayed = process.argv[2];

if(!inFile) {
    console.log("No in file specified.")
    return;
}


var fileArray = fs.readFileSync(inFile, 'utf8').split('\n');
fileArray.splice(0,1);
var fileData = fileArray.join('\n');


var fileObj = Papa.parse(fileData, { header: true })

console.log(fileObj.data);
// ok now..let's convert it.


var output = fileObj.data.map(function(item){

    return {
        Jersey: item['#'],
        MatchGamesPlayed: setsPlayed,
        TotalServes: item['S Att'],
        ServingAces: item['Ace'],
        ServingErrors: item['S Err'],
        ServingPoints: item['S Pts W'],
        AttacksAttempts: item['H Att'],
        AttacksKills: item['Kill'],
        AttacksErrors: item['H Err'],
        ServingReceivedSuccess: item['SR Att'],
        ServingReceivedErrors: item['SR Err'],
        BlocksSolo: item['B Solo'],
        BlocksAssists: item['B Assist'],
        BlocksErrors: 0,
        BallHandlingAttempt: 0,
        Assists: item['Assist'],
        AssistsErrors: item['A Err'],
        Digs: item['Digs'],
        DigsErrors: 0

    };

});


for(var i = 0; i < output.length; i++){
    if(!output[i].Jersey){
        output.splice(i, 1);
    }
}
//console.log(output);

var outFileData = Papa.unparse(output, {header: true, delimiter: '|'});

fs.writeFileSync(inFile + ".txt", outFileData, {encoding: 'utf8'});

console.log(outFileData);